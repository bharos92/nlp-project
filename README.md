
* Stacker:

    ```
    train=train3+dev/ # training data directory, each file therein in *.ann,*.xml,*.txt form 
    embeddings=/data/wordvecs/ExtendedDependencyBasedSkip-gram/wiki_extvec_words # Komninos embeddings 
    cs=4 
    
    python stackedLearner.py ${train} test/scienceie2017_test_unlabelled/ ${embeddings} ${cs} None document > stacker.out 
    
    The predictions will be in the file "stacker.out". Further do: 
    
    ./extract.py < stacker.out > stacker.extracted 
    ./writeout.py test/scienceie2017_test_unlabelled stacker.extracted MY_PRED_DIR > msg 2>err_msg 
    python scripts/eval.py GoldTest/semeval_articles_test/ MY_PRED_DIR rel
    ```
******************

Running on dev set:
python ./stackedLearner.py ./train2 ./scienceie2017_dev/dev/ ./embeddings/glove.6B.50d.txt 4 None document > stacker.out
python extract.py < stacker.out > stacker.extracted 
python writeout.py ./scienceie2017_dev/dev/ stacker.extracted ./pred/ > msg 2 
python ./scripts/eval.py ./scienceie2017_dev/dev/ ./pred/ rel


*******************
* Char-CNN: 

    ```
    baseCMD="convNet.py train3+dev/ test/scienceie2017_test_unlabelled/ empty"
    cs=4 
    L=50 
    M=80 
    R=50 
    nfilter=300 
    filter_length=3
    document=document
    python ${baseCMD} ${cs} ${L} ${M} ${R} ${nfilter} ${filter_length} ${document} > conv.out
    
    The predictions will be in the file "conv.out". Further do: 
    
    ./extract.py < conv.out > conv.extracted 
    ./writeout.py test/scienceie2017_test_unlabelled conv.extracted MY_PRED_DIR > msg 2>err_msg 
    python scripts/eval.py GoldTest/semeval_articles_test/ MY_PRED_DIR rel
    ```

******************

Running on dev set:
python convNet.py ./train2/ ./scienceie2017_dev/dev/ ./embeddings/glove.6B.50d.txt 4 50 80 50 300 3 document > conv.out
python extract.py < conv.out > conv.extracted
python writeout.py ./scienceie2017_dev/dev/ conv.extracted ./pred/ > msg 2 
python ./scripts/eval.py ./scienceie2017_dev/dev/ ./pred/ rel

*******************

* AB-LSTM:
    * Modify `run_blstm.sh` to reflect your data paths, then run it.
    * This will create and run 20 different configurations of the AB-LSTM, and write output ANN files into resp. output folders.
